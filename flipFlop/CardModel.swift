//
//  CardModel.swift
//  flipFlop
//
//  Created by MALLOJJALA PAVAN TEJA on 3/17/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import Foundation

class CardModel {
    func getCards() -> [Card] {
        
        var generatedCards:[Card] = []
        
        for _ in 0...5 {
            let num = Int.random(in: 1...100)
            let cardOne = Card()
            cardOne.num = String(num)
            generatedCards.append(cardOne)
            
            let cardTwo = Card()
            cardTwo.num = String(num)
            generatedCards.append(cardTwo)
        }
        
        let shuffledCards = generatedCards.shuffled()
        return shuffledCards
        
        
    }
}
