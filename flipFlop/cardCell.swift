//
//  cardCell.swift
//  flipFlop
//
//  Created by MALLOJJALA PAVAN TEJA on 3/17/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit

class cardCell: UICollectionViewCell {
    
    
    @IBOutlet weak var back: UILabel!
    
    @IBOutlet weak var front: UILabel!
    
    
    var card: Card?
    
    func setCard(_ card:Card) {
        
        self.card = card
        
        front.text = card.num
        
        if card .isFlipped == true {
            UIView.transition(from: self.back, to: self.front, duration: 0, options: [.transitionFlipFromRight, .showHideTransitionViews]) { (t) in
                
                self.backgroundColor = UIColor.white
                self.back.backgroundColor = UIColor.white
                
            }
        } else {
            
            UIView.transition(from: self.front, to: self.back, duration: 0, options: [.transitionFlipFromRight, .showHideTransitionViews]) { (t) in
                self.back.backgroundColor = UIColor.blue
                self.layer.backgroundColor = UIColor.blue.cgColor
                self.layer.cornerRadius = 10
                self.layer.borderWidth = 4.0
                self.layer.borderColor = UIColor.white.cgColor
                self.layer.backgroundColor = UIColor.blue.cgColor }
            
        }
        
        
    }
    
    
    func flip () {
        
        
        UIView.transition(from: self.back, to: self.front, duration: 0.3, options: [.transitionFlipFromRight, .showHideTransitionViews]) { (t) in
            
            self.backgroundColor = UIColor.white
            self.back.backgroundColor = UIColor.white
            
        }
        
    }
    
    func flop () {
        // to delay  flip back
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            UIView.transition(from: self.front, to: self.back, duration: 0.3, options: [.transitionFlipFromRight, .showHideTransitionViews]) { (t) in
                self.back.backgroundColor = UIColor.blue
                self.layer.backgroundColor = UIColor.blue.cgColor
                self.layer.cornerRadius = 10
                self.layer.borderWidth = 4.0
                self.layer.borderColor = UIColor.white.cgColor
                self.layer.backgroundColor = UIColor.blue.cgColor }
        }
        
        
    }
    
    // once a pair matched
    func matched()  {
        self.front.text = "\(self.front.text ?? "") ✅"
    }

}
