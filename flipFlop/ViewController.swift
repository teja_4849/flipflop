//
//  ViewController.swift
//  flipFlop
//
//  Created by MALLOJJALA PAVAN TEJA on 3/17/20.
//  Copyright © 2020 MALLOJJALAPAVANTEJA. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet weak var stepsCount: UIBarButtonItem!
    @IBOutlet weak var stepsLable: UIBarButtonItem!
    @IBOutlet weak var refreshBtn: UIBarButtonItem!
    @IBOutlet weak var baseView: UICollectionView!

    var model = CardModel()
    var hiddenNumsArr = [Card]()
    var firstFlippedCardIndex:IndexPath?
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        

        loadVMData()
    }
    
    func loadVMData()  {
        self.hiddenNumsArr = model.getCards()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return hiddenNumsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "number", for: indexPath) as! cardCell
        
        cell.layer.cornerRadius = 10
        cell.layer.borderWidth = 4.0
        cell.layer.borderColor = UIColor.white.cgColor
        
        cell.layer.backgroundColor = UIColor.blue.cgColor
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 2.0, height: 4.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        
        cell.back.backgroundColor = UIColor.blue
        cell.front.backgroundColor = UIColor.white
        cell.front.isHidden = true
        
        let card = hiddenNumsArr[indexPath.row]
        
        cell.setCard(card)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = view.frame.size.height
        let width = view.frame.size.width
        return CGSize(width: width * 0.3109, height: height * 0.206)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        counter += 1
        stepsCount.title = "\(counter)"
        let cell = collectionView.cellForItem(at: indexPath) as! cardCell
        let card = hiddenNumsArr[indexPath.row]
        
        if card.isFlipped == false && card.isMatched == false{
            cell.flip()
            card.isFlipped = true
            
            if firstFlippedCardIndex == nil {
                firstFlippedCardIndex = indexPath
                
            } else {
                checkforMatches(secondFlippedIndex: indexPath)
            }
        }
        
    }
    
    func checkforMatches(secondFlippedIndex: IndexPath)  {
        let cardOneCell = baseView.cellForItem(at: firstFlippedCardIndex!) as? cardCell
        
        let cardTwoCell = baseView.cellForItem(at: secondFlippedIndex) as? cardCell
        
        let cardOne = hiddenNumsArr[firstFlippedCardIndex!.row]
        let cardTwo = hiddenNumsArr[secondFlippedIndex.row]
        
        if cardOne.num == cardTwo.num {
            cardOne.isMatched = true
            cardTwo.isMatched = true
            
            cardOneCell?.matched()
            cardTwoCell?.matched()

            
            checkGameEnded()
        } else {
            cardOne.isMatched = false
            cardTwo.isMatched = false
            cardOneCell?.flop()
            cardTwoCell?.flop()
            cardOne.isFlipped = false
            cardTwo.isFlipped = false
            
        }
        firstFlippedCardIndex = nil
    }
    
    
    func checkGameEnded()  {
        
        var isWon = true
        
        for hiddennum in hiddenNumsArr {
            if hiddennum.isMatched == false{
                isWon = false
                return
            }
        }
        
        var title = ""
        var message = ""
        
        if isWon == true {
            title = "Congratulation!"
            message = "you won this game by \(counter)!"
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Try another Round", style: .default) { (action) in
            self.refreshGame()
        }

        alert.addAction(alertAction)
        present(alert, animated: true, completion: nil)
    }
    
    ///  method to refresh the game
    func refreshGame()  {
        loadVMData()
        baseView.reloadData()
        counter = 0
        stepsCount.title = ""
    }

    @IBAction func refreshBtn(_ sender: Any) {
      refreshGame()
    }
}

